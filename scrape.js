var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');


//url = 'http://hcpreports.blogspot.in/2016/05/batch-11.html';
url_main = 'http://www.hemenparekh.in/';

var email_arr = [];
//getEmailFromBatch(url,4);

function getEmailFromBatch(batch_url, i, callback) {
    //var email_arr = [];
    request(batch_url, function(error, response, html) {
        if (!error) {
            var $ = cheerio.load(html);
            $('tr .xl65').filter(function() {
                var temp = $(this).text().replace(/[<>]/g, '*~*').split('*~*');

                var name = temp[0];
                var mail_id = temp[1];

                var temp_obj = {};
                temp_obj.name = name;
                temp_obj.mail_id = mail_id

                email_arr.push(temp_obj);
                //console.log(temp_obj);
            })
            return callback(null, i);
            //fs.writeFileSync('mail_json_' + i + '.json', JSON.stringify(email_arr));
        } else {
            return callback(i, null);
        }
    })
}

getBatchURLs(function(error, data) {
    if (error) {
        console.log('OH NO');
    } else {
        var batch_url = '';
        var i = 0;
        console.log('Pages having mail: ' + data.length);
        for (var x = Math.ceil(data.length*(9/10)); x < Math.ceil(data.length); x++) {
            batch_url = data[x];
            console.log(batch_url);
            getEmailFromBatch(batch_url, x, function(error, result) {
                if (result) {
                    console.log(result + '--------' + x + '--------' + email_arr.length);
                    fs.writeFileSync('emails9.json', JSON.stringify(email_arr));
                } else {
                    console.log('failed ' + x);
                }
            });
        }
        //fs.writeFileSync('emails.json', email_arr);
    }
    // fs.writeFileSync('myjsonfile.json', email_arr);
    // console.log(email_arr.length)
});

function getBatchURLs(callback) {
    var batch_arr = [];
    request(url_main, function(error, response, html) {
        if (!error) {
            var $ = cheerio.load(html);

            $('a').filter(function() {
                batch_arr.push($(this).attr('href'));
            })

            var filtered_batch_arr = batch_arr.filter(function(item) {
                return /hcpreports.blogspot.in/.test(item);
            });
            //console.log(filtered_batch_arr);

            return callback(null, filtered_batch_arr);
        } else {
            return callback(error, null);
        }
    })
}