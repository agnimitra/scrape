var jsonexport = require('jsonexport');
var fs = require('fs');

var contacts = JSON.parse(fs.readFileSync('emails4.json', 'utf8'));

jsonexport(contacts, function(err, csv) {
    if (err) return console.log(err);
    fs.writeFileSync('emails4.csv', csv)
});